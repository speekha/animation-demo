package fr.speekha.animationdemo.drawable.svg

import android.graphics.Paint

data class PathShape(
        val name: String,
        val pathData: String,
        var fillColor: Int,
        var strokeWidth: Float,
        var strokeColor: Int,
        var strokeLineCap: Paint.Cap
) {
    val path = PathParser().parse(pathData)
}