package fr.speekha.animationdemo.drawable.svg

import android.graphics.Path
import android.util.Log
import java.util.regex.Pattern

class PathParser {

    val pattern = Pattern.compile("[a-zA-Z]( -?\\d+(\\.\\d+)?)*")

    fun parse(pathData: String): Path {
        val path = Path()
        val matcher = pattern.matcher(pathData)
        var index = 0

        while (matcher.find(index)) {
            val token = pathData.substring(matcher.start() until matcher.end())
            index = matcher.end()
            parseCommand(path, token.trim())
        }

        return path
    }

    private fun parseCommand(path: Path, data: String) {
        Log.d("Parser", "Parsing $data")
        val tokens = data.split(" ")
        var index = 0
        try {

            when (tokens[index++].toUpperCase()) {
                "M" -> parseMoveTo(path, tokens)
                "Z" -> path.close()
                "L" -> parseLineTo(path, tokens)
                "Q" -> parseQuadraticTo(path, tokens)
                "C" -> parseCubicTo(path, tokens)
                else -> Log.w("Path parser", "Unsupported operation: ${tokens[index - 1]}")
            }
        } catch (t: Throwable) {
            Log.e("Parser", "Error parsing $data", t)
        }
    }

    private fun parseMoveTo(path: Path, tokens: List<String>) {
        var x = tokens[1].toFloat()
        var y = tokens[2].toFloat()
        Log.d("Path parser", "Move to $x, $y")
        if (tokens[0] == "M") {
            path.moveTo(x, y)
        } else {
            path.rMoveTo(x, y)
        }
        for (i in 3..tokens.lastIndex step 2) {
            x = tokens[i].toFloat()
            y = tokens[i + 1].toFloat()
            Log.d("Path parser", "Line to $x, $y")
            if (tokens[0] == "L") {
                path.lineTo(x, y)
            } else {
                path.rLineTo(x, y)
            }
        }
    }

    private fun parseLineTo(path: Path, tokens: List<String>) {
        for (i in 1..tokens.lastIndex step 2) {
            val x = tokens[i].toFloat()
            val y = tokens[i + 1].toFloat()
            Log.d("Path parser", "Line to $x, $y")
            path.lineTo(x, y)
        }
    }

    private fun parseQuadraticTo(path: Path, tokens: List<String>) {
        for (i in 1..tokens.lastIndex step 6) {
            val x1 = tokens[i]
            val y1 = tokens[i + 1]
            val x = tokens[i + 2]
            val y = tokens[i + 3]
            Log.d("Path parser", "Quadratic Bezier line to $x1, $y1, $x, $y")
            if (tokens[0] == "Q") {
                path.quadTo(x1.toFloat(), y1.toFloat(), x.toFloat(), y.toFloat())
            } else {
                path.rQuadTo(x1.toFloat(), y1.toFloat(), x.toFloat(), y.toFloat())
            }
        }
    }
    private fun parseCubicTo(path: Path, tokens: List<String>) {
        for (i in 1..tokens.lastIndex step 6) {
            val x1 = tokens[i]
            val y1 = tokens[i + 1]
            val x2 = tokens[i + 2]
            val y2 = tokens[i + 3]
            val x3 = tokens[i + 4]
            val y3 = tokens[i + 5]
            Log.d("Path parser", "Cubic Bezier line to $x1, $y1, $x2, $y2, $x3, $y3")
            if (tokens[0] == "C") {
                path.cubicTo(x1.toFloat(), y1.toFloat(), x2.toFloat(), y2.toFloat(), x3.toFloat(), y3.toFloat())
            } else {
                path.rCubicTo(x1.toFloat(), y1.toFloat(), x2.toFloat(), y2.toFloat(), x3.toFloat(), y3.toFloat())
            }
        }
    }
}