package fr.speekha.animationdemo.drawable.svg

import android.content.Context
import android.content.res.XmlResourceParser
import android.graphics.Paint
import org.xmlpull.v1.XmlPullParser

class SvgParser(private val context: Context) {

    private val pathParser = PathParser()


    fun inflateSvgResource(res: Int): SvgImage {
        val parser: XmlResourceParser = context.resources.getXml(res)

        val image = SvgImage()

        var type: Int = parser.next()
        while (type != XmlPullParser.START_TAG && type != XmlPullParser.END_DOCUMENT) {
            type = parser.next()
        }

        while (type != XmlPullParser.END_DOCUMENT) {
            processEvent(type, parser, image)
            type = parser.nextToken()
        }
        return image
    }

    private fun processEvent(type: Int, parser: XmlResourceParser, image: SvgImage) {
        if (type == XmlPullParser.START_TAG) {
            val tagName = parser.name
            when (tagName) {
                VECTOR_TAG -> image.parseVector(parser)
                SHAPE_PATH_TAG -> image.paths += parsePath(parser)
            }
        }
    }

    private fun SvgImage.parseVector(parser: XmlResourceParser) {
        width = parser.getAttributeIntValue(ANDROID_NAMESPACE, WIDTH_ATTRIBUTE,0)
        height = parser.getAttributeIntValue(ANDROID_NAMESPACE, HEIGHT_ATTRIBUTE, 0)
        viewportWidth = parser.getAttributeFloatValue(ANDROID_NAMESPACE, VIEWPORT_WIDTH_ATTRIBUTE, 0f)
        viewportHeight = parser.getAttributeFloatValue(ANDROID_NAMESPACE, VIEWPORT_HEIGHT_ATTRIBUTE, 0f)
    }


    private fun parsePath(parser: XmlResourceParser): PathShape {
        val name = parser.getAttributeValue(ANDROID_NAMESPACE, NAME_ATTRIBUTE) ?: ""
        val data = parser.getAttributeValue(ANDROID_NAMESPACE, PATH_DATA_ATTRIBUTE) ?: ""
        val fillColor = parser.getAttributeIntValue(ANDROID_NAMESPACE, FILL_COLOR_ATTRIBUTE, 0)
        val strokeWidth = parser.getAttributeFloatValue(ANDROID_NAMESPACE, STROKE_WIDTH_ATTRIBUTE, 0.0f)
        val strokeColor = parser.getAttributeIntValue(ANDROID_NAMESPACE, STROKE_COLOR_ATTRIBUTE, 0)
        val strokeLineCap = Paint.Cap.values()[parser.getAttributeIntValue(ANDROID_NAMESPACE, STROKE_LINE_CAP, 0)]
        return PathShape(name, data, fillColor, strokeWidth, strokeColor, strokeLineCap)
    }
}