package fr.speekha.animationdemo.drawable.svg

class SvgImage(
        var width: Int = 0,
        var height: Int = 0,
        var viewportWidth: Float = 0f,
        var viewportHeight: Float = 0f
) {

    var paths = emptyList<PathShape>()

}