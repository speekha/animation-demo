package fr.speekha.animationdemo.drawable

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ValueAnimator
import android.view.animation.AccelerateDecelerateInterpolator

class LoaderAnimator(
    val drawable: LoadAndFinishDrawable
) {
    enum class State {
        IDLE, WAITING, FINISH_TRIM_END, FINISH_TRIM_START, DRAW_TICK
    }

    val loaderDuration = 1000L
    val offset = 200L
    val finishDuration = 500L
    private var success = true
    private val animStart = buildLoadingAnimation(0, ::updateTrimStart, ::onRepeatTrimStart)
    private val animEnd = buildLoadingAnimation(offset, ::updateTrimEnd, ::onRepeatTrimEnd)
    private lateinit var animFinish: AnimatorSet

    private var state = State.IDLE

    fun start() {
        state = State.WAITING
        stopAllAnimations()
        startLoadingAnimation()
    }

    fun finish(result: Boolean = true) {
        state = State.FINISH_TRIM_END
        success = result
        drawable.setResult(result)
    }

    private fun startLoadingAnimation() {
        drawable.initState()
        animStart.start()
        animEnd.start()
    }

    private fun startFinalAnimation() {
        animStart.cancel()
        state = State.DRAW_TICK
        animFinish = buildFinishAnimation()
        animFinish.start()
    }

    private fun stopAllAnimations() {
        animStart.cancel()
        animEnd.cancel()
        if (state == State.DRAW_TICK) {
            animFinish.cancel()
        }
    }

    private fun buildLoadingAnimation(
        offset: Long,
        updateListener: (ValueAnimator) -> Unit,
        repeatListener: () -> Unit
    ) = ValueAnimator.ofFloat(1f, 0f).apply {
        duration = loaderDuration
        startDelay = offset
        repeatCount = ValueAnimator.INFINITE
        repeatMode = ValueAnimator.RESTART
        addUpdateListener(updateListener)
        addListener(AnimListener(onRepeat = repeatListener))
    }

    private fun buildFinishAnimation() = AnimatorSet().apply {
        val interpolator = AccelerateDecelerateInterpolator()
        val circle = if (success) drawable.successColor else drawable.errorColor
        val fill = if (success) drawable.successFillColor else drawable.errorFillColor
        playTogether(
            buildTickAnimation(interpolator),
            buildFillAnimation(interpolator, fill),
            buildCircleFadeAnimation(interpolator, circle)
        )
    }

    private fun buildTickAnimation(clock: AccelerateDecelerateInterpolator) =
        ValueAnimator.ofFloat(0f, 1f).apply {
            duration = finishDuration
            repeatCount = 0
            interpolator = clock
            addUpdateListener { drawable.updateTick(it.animatedValue as Float) }
        }

    private fun buildFillAnimation(clock: AccelerateDecelerateInterpolator, color: Int) =
        ValueAnimator.ofArgb(drawable.fillColor, color).apply {
            duration = finishDuration
            repeatCount = 0
            interpolator = clock
            addUpdateListener { drawable.updateFill(it.animatedValue as Int) }
        }

    private fun buildCircleFadeAnimation(clock: AccelerateDecelerateInterpolator, color: Int) =
        ValueAnimator.ofArgb(drawable.circleColor, color).apply {
            duration = finishDuration
            repeatCount = 0
            interpolator = clock
            addUpdateListener {
                drawable.updateCircleColor(it.animatedValue as Int)
            }
        }

    private fun updateTrimStart(animator: ValueAnimator) {
        if (state != State.DRAW_TICK) {
            val value = animator.animatedValue as Float
            drawable.updateTrimStart(value)
        }
    }

    private fun updateTrimEnd(animator: ValueAnimator) {
        val value = animator.animatedValue as Float
        drawable.updateTrimEnd(value)
    }

    private fun onRepeatTrimStart() {
        if (state == State.FINISH_TRIM_START) {
            startFinalAnimation()
        }
    }

    private fun onRepeatTrimEnd() {
        if (state == State.FINISH_TRIM_END) {
            animEnd.cancel()
            state = State.FINISH_TRIM_START
        }
    }

    class AnimListener(
        val onStart: () -> Unit = {},
        val onRepeat: () -> Unit = {},
        val onEnd: () -> Unit = {},
        val onCancel: () -> Unit = {}
    ) : Animator.AnimatorListener {

        override fun onAnimationStart(animation: Animator?) = onStart()

        override fun onAnimationRepeat(animation: Animator?) = onRepeat()

        override fun onAnimationEnd(animation: Animator?) = onEnd()

        override fun onAnimationCancel(animation: Animator?) = onCancel()

    }
}