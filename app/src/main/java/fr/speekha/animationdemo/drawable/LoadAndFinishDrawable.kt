package fr.speekha.animationdemo.drawable

import android.graphics.Canvas
import android.graphics.ColorFilter
import android.graphics.DashPathEffect
import android.graphics.Matrix
import android.graphics.Paint
import android.graphics.Path
import android.graphics.PathMeasure
import android.graphics.PixelFormat
import android.graphics.drawable.Drawable
import fr.speekha.animationdemo.drawable.svg.PathShape
import fr.speekha.animationdemo.drawable.svg.SvgImage
import kotlin.math.max
import kotlin.math.min

class LoadAndFinishDrawable(svg: SvgImage) : Drawable() {


    private val pathCircle = svg.paths.first { it.name == "circle" }
    private val pathTick = svg.paths.first { it.name == "tick" }
    private val pathCross1 = svg.paths.first { it.name == "cross_1" }
    private val pathCross2 = svg.paths.first { it.name == "cross_2" }

    val successColor = 0xff00e000.toInt()
    val errorColor = 0xfff00000.toInt()
    val successFillColor = 0xffe0ffe0.toInt()
    val errorFillColor = 0xffffa0a0.toInt()
    val circleColor = pathCircle.strokeColor
    val fillColor = pathCircle.fillColor

    private var width: Float = svg.viewportWidth
    private var height: Float = svg.viewportHeight

    private var trimStart = 0f
    private var trimEnd = 0f
    private var drawTick: Boolean = true
    private var finalProgress = 0f

    private var _alpha: Int = 255
    private var _colorFilter: ColorFilter? = null

    fun initState() {
        finalProgress = 0f
        pathCircle.fillColor = 0xffffffff.toInt()
        pathCircle.strokeColor = 0xff0033aa.toInt()
    }

    fun updateCircleColor(color: Int) {
        pathCircle.strokeColor = color
    }

    fun updateTrimStart(value: Float) {
        trimStart = value
        invalidateSelf()
    }

    fun updateTrimEnd(value: Float) {
        trimEnd = value
        invalidateSelf()
    }

    fun updateTick(value: Float) {
        finalProgress = value
        invalidateSelf()
    }

    fun updateFill(value: Int) {
        pathCircle.fillColor = value
        invalidateSelf()
    }

    fun setResult(result: Boolean) {
        drawTick = result
    }

    override fun setAlpha(alpha: Int) {
        _alpha = alpha
    }

    override fun setColorFilter(colorFilter: ColorFilter?) {
        _colorFilter = colorFilter
    }

    override fun getOpacity(): Int = PixelFormat.TRANSLUCENT

    override fun draw(cnv: Canvas?) {
        cnv?.let { canvas ->
            if (width.toInt() != canvas.width && height.toInt() != canvas.height) {
                scaleImage(canvas)
            }
            drawPaths(canvas)
        }
    }

    private fun scaleImage(canvas: Canvas) {
        val scale = computeScale(canvas)
        scaleStrokes(scale)
        transformPaths(canvas) { computeScaleMatrix(scale) }
        transformPaths(canvas) { computeTranslationMatrix(it) }
    }

    private fun computeScale(canvas: Canvas) =
        min(canvas.width.toFloat() / width, canvas.height.toFloat() / height)

    private fun scaleStrokes(scale: Float) {
        pathCircle.strokeWidth *= scale
        pathTick.strokeWidth *= scale
        pathCross1.strokeWidth *= scale
        pathCross2.strokeWidth *= scale
    }

    private fun transformPaths(canvas: Canvas, computeMatrix: (Canvas) -> Matrix) {
        val translateMatrix = computeMatrix(canvas)
        pathCircle.path.transform(translateMatrix)
        pathTick.path.transform(translateMatrix)
        pathCross1.path.transform(translateMatrix)
        pathCross2.path.transform(translateMatrix)
    }

    private fun computeScaleMatrix(scale: Float): Matrix {
        width *= scale
        height *= scale
        return Matrix().apply {
            setScale(scale, scale, 0f, 0f)
        }
    }

    private fun computeTranslationMatrix(canvas: Canvas): Matrix {
        return Matrix().apply {
            setTranslate(
                centerPosition(canvas.width, width),
                centerPosition(canvas.height, height)
            )
        }
    }

    private fun centerPosition(canvas: Int, size: Float) = (canvas - size) / 2

    private fun drawPaths(canvas: Canvas) {
        drawPath(pathCircle, trimStart..trimEnd, canvas)
        if (drawTick) {
            drawPath(pathTick, 0f..finalProgress, canvas)
        } else {
            drawPath(pathCross1, 0f..min(2 * finalProgress, 1f), canvas)
            drawPath(pathCross2, 0f..2 * max(0f, finalProgress - 0.5f), canvas)
        }
    }

    private fun drawPath(shape: PathShape, range: ClosedRange<Float>, canvas: Canvas) {
        if (range.endInclusive != range.start) {
            fillBackground(shape, canvas)
            drawOutline(shape, range, canvas)
        }
    }

    private fun fillBackground(shape: PathShape, canvas: Canvas) {
        if (shape.fillColor != 0x000000) {
            val paint = getFillPaint(shape)
            canvas.drawPath(shape.path, paint)
        }
    }

    private fun drawOutline(shape: PathShape, range: ClosedRange<Float>, canvas: Canvas) {
        val length = measurePathLength(shape.path)
        val dashes = getDashes(range.start, range.endInclusive).map { it * length }.toFloatArray()
        val paint = getLinePaint(dashes, shape)
        canvas.drawPath(shape.path, paint)
    }

    private fun measurePathLength(path: Path): Float {
        val pathMeasure = PathMeasure()
        pathMeasure.setPath(path, false)
        return pathMeasure.length
    }

    private fun getDashes(start: Float, end: Float): FloatArray = when {
        start == 0f -> floatArrayOf(end, 1f)
        start > end -> floatArrayOf(end, start - end, 1 - start, 1f)
        else -> floatArrayOf(0f, start, end - start, 1f)
    }

    private fun getLinePaint(dashes: FloatArray, shape: PathShape) =
        Paint(Paint.ANTI_ALIAS_FLAG).apply {
            style = Paint.Style.STROKE
            strokeWidth = shape.strokeWidth
            strokeCap = shape.strokeLineCap
            color = shape.strokeColor
            pathEffect = getDashEffect(dashes)
            alpha = _alpha
            colorFilter = _colorFilter
        }

    private fun getDashEffect(dashes: FloatArray) = if (dashes[0] == 0f) {
        DashPathEffect(dashes, 0f)
    } else {
        DashPathEffect(dashes, 0f)
    }

    private fun getFillPaint(shape: PathShape) = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.FILL
        color = shape.fillColor
        alpha = _alpha
        colorFilter = _colorFilter
    }
}