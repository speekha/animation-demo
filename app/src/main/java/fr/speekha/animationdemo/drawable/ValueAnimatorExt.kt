package fr.speekha.animationdemo.drawable

import android.animation.Animator
import android.animation.ValueAnimator
import io.reactivex.Observable
import io.reactivex.ObservableEmitter

sealed class Event
object StartEvent : Event()
object RepeatEvent : Event()
class UpdateEvent<out T : Any>(val value: T) : Event()
object CancelEvent : Event()

@Suppress("UNCHECKED_CAST")
fun <T : Any> ValueAnimator.toObservable() = Observable.create<Event> { emitter ->
    addListener(object : Animator.AnimatorListener {
        override fun onAnimationRepeat(animation: Animator?) = emitter.safeNext(RepeatEvent)
        override fun onAnimationEnd(animation: Animator?) = emitter.safeComplete()
        override fun onAnimationCancel(animation: Animator?) = emitter.safeNext(CancelEvent)
        override fun onAnimationStart(animation: Animator?) = emitter.safeNext(StartEvent)
    })
    addUpdateListener {
        try {
            emitter.safeNext(UpdateEvent(it as T))
        } catch (t: Throwable) {
            emitter.safeError(t)
        }
    }
}.doOnSubscribe {
    start()
}.doOnDispose {
    cancel()
}

private fun <T> ObservableEmitter<T>.safeNext(value: T) {
    if (!isDisposed) {
        onNext(value)
    }
}

private fun ObservableEmitter<*>.safeComplete() {
    if (!isDisposed) {
        onComplete()
    }
}

private fun ObservableEmitter<*>.safeError(t: Throwable) {
    if (!isDisposed) {
        onError(t)
    }
}