package fr.speekha.animationdemo.drawable.svg

const val ANDROID_NAMESPACE = "http://schemas.android.com/apk/res/android"

const val VECTOR_TAG = "vector"
const val WIDTH_ATTRIBUTE = "width"
const val HEIGHT_ATTRIBUTE = "height"
const val VIEWPORT_WIDTH_ATTRIBUTE = "viewportWidth"
const val VIEWPORT_HEIGHT_ATTRIBUTE = "viewportHeight"

const val SHAPE_PATH_TAG = "path"
const val NAME_ATTRIBUTE = "name"
const val PATH_DATA_ATTRIBUTE = "pathData"
const val FILL_COLOR_ATTRIBUTE = "fillColor"
const val STROKE_WIDTH_ATTRIBUTE = "strokeWidth"
const val STROKE_COLOR_ATTRIBUTE = "strokeColor"
const val STROKE_LINE_CAP = "strokeLineCap"
