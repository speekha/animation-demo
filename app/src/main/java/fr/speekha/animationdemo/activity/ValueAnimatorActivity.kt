package fr.speekha.animationdemo.activity

import android.animation.AnimatorInflater
import android.animation.ValueAnimator
import android.os.Bundle
import android.util.Log
import android.view.View
import fr.speekha.animationdemo.R
import fr.speekha.extensions.app.startActivity
import kotlinx.android.synthetic.main.xml_anim_activity.*

class ValueAnimatorActivity : AnimationActivity() {

    // Since API 11
    // android.animation
    // Very generic : allows to animate anything
    // Offers simple computation service to generate animated values but corresponding code for UI
    // modification must be provided as an animator update listener
    // https://developer.android.com/guide/topics/graphics/prop-animation.html#choreography
    // https://android-developers.googleblog.com/2011/02/animation-in-honeycomb.html

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.xml_anim_activity)

        val xmlAnim = AnimatorInflater.loadAnimator(applicationContext, R.animator.blink_animator) as ValueAnimator
        xmlAnim.addUpdateListener(AnimListener(xmlAnimated, "XML Animation"))

        val codeAnim = buildAnimation()
        codeAnim.addUpdateListener(AnimListener(codeAnimated, "Code Animation"))

        start.setOnClickListener {
            xmlAnim.start()
            codeAnim.start()
        }

        stop.setOnClickListener {
            xmlAnim.cancel()
            codeAnim.cancel()
        }

        next.setOnClickListener {
            startActivity<ObjectAnimatorActivity>()
        }
    }

    private fun buildAnimation() = ValueAnimator.ofFloat(0.0f, 0.75f, 0.25f, 1.0f).apply {
        duration = resources.getInteger(R.integer.long_anim_duration).toLong()
        repeatCount = ValueAnimator.INFINITE
        repeatMode = ValueAnimator.REVERSE
    }

    inner class AnimListener(val view: View, val tag: String) : ValueAnimator.AnimatorUpdateListener {
        override fun onAnimationUpdate(animation: ValueAnimator?) {
            animation?.let {
                view.alpha = animation.animatedValue as Float
                Log.d(tag, "Value : ${animation.animatedValue}")
            }
        }
    }
}
