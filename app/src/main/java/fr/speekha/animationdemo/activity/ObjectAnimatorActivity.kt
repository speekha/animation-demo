package fr.speekha.animationdemo.activity

import android.animation.AnimatorInflater
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.os.Bundle
import android.view.View
import fr.speekha.animationdemo.R
import fr.speekha.extensions.app.startActivity
import kotlinx.android.synthetic.main.xml_anim_activity.*

class ObjectAnimatorActivity : AnimationActivity() {

    // Since API 11
    // Extension of the value animator that handles object animations without the need for
    // a listener. Animated property name is provided and ObjectAnimator will try to invoke a
    // set<Property> method to update it as needed (through reflection or JNI)
    // No extra listener, but cost of reflection.
    // https://android-developers.googleblog.com/2011/05/introducing-viewpropertyanimator.html

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.xml_anim_activity)

        val xmlAnim = AnimatorInflater.loadAnimator(applicationContext, R.animator.object_blink_animator) as ObjectAnimator
        xmlAnim.target = xmlAnimated
        val codeAnim = buildAnimation(codeAnimated)

        start.setOnClickListener {
            xmlAnim.start()
            codeAnim.start()
        }

        stop.setOnClickListener {
            xmlAnim.cancel()
            codeAnim.cancel()
        }

        next.setOnClickListener {
            startActivity<ViewPropertyAnimatorActivity>()
        }
    }

    private fun buildAnimation(view: View) = ObjectAnimator.ofFloat(view, "alpha", 0.0f, 0.75f, 0.25f, 1.0f).apply {
        duration = resources.getInteger(R.integer.long_anim_duration).toLong()
        repeatCount = ValueAnimator.INFINITE
        repeatMode = ValueAnimator.REVERSE
    }
}
