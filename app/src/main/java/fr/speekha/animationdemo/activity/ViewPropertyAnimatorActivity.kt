package fr.speekha.animationdemo.activity

import android.os.Bundle
import android.view.View
import android.view.ViewPropertyAnimator
import fr.speekha.animationdemo.R
import fr.speekha.extensions.app.startActivity
import kotlinx.android.synthetic.main.xml_anim_activity.*

class ViewPropertyAnimatorActivity : AnimationActivity() {

    var anim: ViewPropertyAnimator? = null

    // Since API 12
    // Simpler syntax
    // Better performance for simultaneous animations
    // https://android-developers.googleblog.com/2011/05/introducing-viewpropertyanimator.html

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.xml_anim_activity)

        xmlAnimated.visibility = View.GONE

        start.setOnClickListener {
            hideView()
        }

        stop.setOnClickListener {
            anim?.cancel()
        }

        next.setOnClickListener {
            startActivity<AnimatedLayoutActivity>()
        }
    }

    private fun hideView() {
        anim = codeAnimated.animate()
                .alpha(0.0f)
                .setDuration(resources.getInteger(R.integer.anim_duration).toLong())
                .withEndAction(this::showView)
        anim?.start()
    }

    private fun showView() {
        anim = codeAnimated.animate()
                .alpha(1.0f)
                .setDuration(resources.getInteger(R.integer.anim_duration).toLong())
                .withEndAction(this::rotateView)
        anim?.start()
    }

    private fun rotateView() {
        anim = codeAnimated.animate()
                .rotation(360.0f)
                .setDuration(500)
                .withEndAction {
                    codeAnimated.rotation = 0.0f
                    hideView()
                }
        anim?.start()
    }
}
