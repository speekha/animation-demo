package fr.speekha.animationdemo.activity

import android.os.Bundle
import fr.speekha.animationdemo.R
import fr.speekha.animationdemo.drawable.LoadAndFinishDrawable
import fr.speekha.animationdemo.drawable.LoaderAnimator
import fr.speekha.animationdemo.drawable.svg.SvgParser
import fr.speekha.extensions.app.startActivity
import kotlinx.android.synthetic.main.custom_drawable_activity.*

class CustomDrawableActivity : AnimationActivity() {

    // A custom drawable using path tracing can be a great tool for certain types of animations.
    // https://medium.com/@crafty/playing-with-paths-3fbc679a6f77
    // More on path tracing by Romain Guy:
    // http://www.curious-creature.com/2013/12/21/android-recipe-4-path-tracing/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.custom_drawable_activity)

        val svg = SvgParser(this).inflateSvgResource(R.drawable.vd_confirm)

        val drawable = LoadAndFinishDrawable(svg)
        val animator = LoaderAnimator(drawable)
        animView.setImageDrawable(drawable)

        start.setOnClickListener { animator.start() }
        success.setOnClickListener { animator.finish() }
        failure.setOnClickListener { animator.finish(false) }

        next.setOnClickListener {
            startActivity<LottieActivity>()
        }
    }
}
