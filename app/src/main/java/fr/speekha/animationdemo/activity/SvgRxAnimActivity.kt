package fr.speekha.animationdemo.activity

import android.os.Bundle
import android.support.graphics.drawable.AnimatedVectorDrawableCompat
import android.util.Log
import android.view.View
import com.jakewharton.rxbinding2.view.RxView
import fr.speekha.animationdemo.R
import fr.speekha.extensions.app.startActivity
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.svg_anim_activity.*
import java.util.concurrent.TimeUnit

class SvgRxAnimActivity : AnimationActivity() {

    // Usable down API 14 thanks to AppCompat lib

    private lateinit var animWait: AnimatedVectorDrawableCompat
    private lateinit var animFinish: AnimatedVectorDrawableCompat
    private var subscription = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.svg_anim_activity)

        animWait = AnimatedVectorDrawableCompat.create(this, R.drawable.avd_wait)?.apply {
            isAutoMirrored = true
        } ?: error("Animation could not be loaded")

        animFinish = AnimatedVectorDrawableCompat.create(this, R.drawable.avd_confirm)?.apply {
            isAutoMirrored = true
        } ?: error("Animation could not be loaded")

        playWithRxSync()

        updateButtons(false)
        next.setOnClickListener {
            startActivity<CustomDrawableActivity>()
        }
    }

    private fun updateButtons(running: Boolean) {
        stop.isEnabled = running
        start.isEnabled = !running
    }

    private fun playWithRxSync() {
        val sampled = prepareTicking()

        start.setOnClickListener {
            updateButtons(true)
            playWaitAnimation()
            subscription.add(sampled.subscribe { playFinishAnimation() })
        }
    }

    private fun prepareTicking(): Observable<Any> {
        val duration = resources.getInteger(R.integer.long_anim_duration).toLong()
        val ticks = Observable.interval(duration, TimeUnit.MILLISECONDS).doOnNext { Log.d("Ticker", "Tick $it") }
        val clicks = RxView.clicks(stop)
        return clicks.sample(ticks).observeOn(AndroidSchedulers.mainThread())
    }

    private fun playWaitAnimation() {
        animWait.stop()
        wait.setImageDrawable(animWait)
        finish.visibility = View.INVISIBLE
        finish.setImageDrawable(animFinish)
        animWait.start()
    }

    private fun playFinishAnimation() {
        animFinish.start()
        finish.visibility = View.VISIBLE
        subscription.clear()
        updateButtons(false)
    }

}
