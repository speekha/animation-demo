package fr.speekha.animationdemo.activity

import android.os.Bundle
import android.support.graphics.drawable.AnimatedVectorDrawableCompat
import android.util.Log
import android.view.View
import com.jakewharton.rxbinding2.view.RxView
import fr.speekha.animationdemo.R
import fr.speekha.extensions.app.startActivity
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.svg_anim_activity.*
import java.util.concurrent.TimeUnit

class SvgAnimActivity : AnimationActivity() {

    // Usable down API 14 thanks to AppCompat lib

    private lateinit var animWait: AnimatedVectorDrawableCompat
    private lateinit var animFinish: AnimatedVectorDrawableCompat

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.svg_anim_activity)

        animWait = AnimatedVectorDrawableCompat.create(this, R.drawable.avd_wait)?.apply {
            isAutoMirrored = true
        } ?: error("Animation could not be loaded")

        animFinish = AnimatedVectorDrawableCompat.create(this, R.drawable.avd_confirm)?.apply {
            isAutoMirrored = true
        } ?: error("Animation could not be loaded")

        playWithoutSync()

        next.setOnClickListener {
            startActivity<SvgRxAnimActivity>()
        }
    }

    private fun playWithoutSync() {
        start.setOnClickListener {
            wait.setImageDrawable(animWait)
            wait.visibility = View.VISIBLE
            finish.visibility = View.INVISIBLE
            finish.setImageDrawable(animFinish)
            animWait.start()
        }

        stop.setOnClickListener {
            wait.visibility = View.INVISIBLE
            animWait.stop()
            finish.visibility = View.VISIBLE
            animFinish.start()
        }
    }

}
