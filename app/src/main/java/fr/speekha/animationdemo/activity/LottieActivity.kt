package fr.speekha.animationdemo.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.util.Log
import com.airbnb.lottie.LottieComposition
import com.airbnb.lottie.LottieDrawable
import fr.speekha.animationdemo.R
import kotlinx.android.synthetic.main.lottie_activity.*

class LottieActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.lottie_activity)

        initAnimationView()

        start.setOnClickListener {
            if (animationView.progress == 1f) {
                animationView.progress = 0f
            }
            animationView.resumeAnimation()
        }

        stop.setOnClickListener {
            animationView.pauseAnimation()
        }

        next.isEnabled = false
    }

    private fun initAnimationView() {
        animationView.repeatCount = LottieDrawable.INFINITE
        animationView.imageAssetsFolder = null
        LottieComposition.Factory.fromAssetFileName(this, "lottiefiles.com - ATM.json", { composition ->
            if (composition == null) {
                onLoadError()
            } else {
                animationView.setComposition(composition)
                animationView.enableMergePathsForKitKatAndAbove(true)
            }
        })
    }

    private fun onLoadError() = Snackbar.make(animationView, "Failed to load animation", Snackbar.LENGTH_LONG).show()

}
