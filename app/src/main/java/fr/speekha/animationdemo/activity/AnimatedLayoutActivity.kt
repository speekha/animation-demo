package fr.speekha.animationdemo.activity

import android.animation.LayoutTransition
import android.os.Bundle
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.util.Pair
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageButton
import android.widget.TextView
import fr.speekha.animationdemo.R
import fr.speekha.extensions.app.startActivity
import kotlinx.android.synthetic.main.animated_layout_activity.*


class AnimatedLayoutActivity : AnimationActivity() {

    // Since API 11, with improvements in API 16 (new supported transition changes)
    // ViewGroup.animateLayoutChanges = true/false
    // https://developer.android.com/training/animation/layout.html
    // https://proandroiddev.com/the-little-secret-of-android-animatelayoutchanges-e4caab2fddec

    // Transition with names in XML since API 21

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.animated_layout_activity)

        container.layoutTransition.enableTransitionType(LayoutTransition.CHANGING)

        add.setOnClickListener {
            addItem()
        }

        next.setOnClickListener {
            val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this,
                    transitionInfo(bugdroid),
                    transitionInfo(desc))
            startActivity<ConstraintLayoutActivity>(options.toBundle())
        }
    }

    private fun transitionInfo(view: View): Pair<View, String> = Pair.create(view, view.transitionName)

    private fun addItem() {
        val newView = LayoutInflater.from(this)
                .inflate(R.layout.list_item_example, container, false).apply {
            findViewById<TextView>(android.R.id.text1).text = getString(R.string.item, container.childCount)
            findViewById<ImageButton>(R.id.delete_button).setOnClickListener {
                container.removeView(this)
            }
        }
        container.addView(newView, 0)
    }
}
