package fr.speekha.animationdemo.activity

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.constraint.ConstraintSet
import android.transition.TransitionManager
import android.view.LayoutInflater
import android.view.View
import android.widget.ArrayAdapter
import fr.speekha.animationdemo.R
import fr.speekha.extensions.app.startActivity
import kotlinx.android.synthetic.main.constraint_layout_activity.*

class ConstraintLayoutActivity : AnimationActivity() {

    // Since API 19
    // Automatic transitions when applying a new set of constraints to a ConstraintLayout

    val constraintSetA = ConstraintSet()
    val constraintSetB = ConstraintSet()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.constraint_layout_activity)

        initList()

        initConstraintSets()
        layoutA.setOnClickListener(changeConstraints(constraintSetA))
        layoutB.setOnClickListener(changeConstraints(constraintSetB))

        next.setOnClickListener {
            startActivity<SvgAnimActivity>()
        }

    }

    fun initList() {
        val data = Array(50) {"Item $it"}
        list.adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, data)
    }

    @SuppressLint("NewApi")
    private fun changeConstraints(constraints: ConstraintSet): (View) -> Unit {
        return {
            TransitionManager.beginDelayedTransition(container)
            constraints.applyTo(container)
        }
    }

    private fun initConstraintSets() {
        constraintSetA.clone(container)
        val variant = LayoutInflater.from(this).inflate(R.layout.constraint_layout_variant, container, false) as ConstraintLayout
        constraintSetB.clone(variant)
    }


}
