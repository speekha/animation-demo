package fr.speekha.animationdemo.activity

import android.support.v7.app.AppCompatActivity

abstract class AnimationActivity : AppCompatActivity() {

    override fun finish() {
        super.finish()
        overridePendingTransition(0, 0)
    }
}