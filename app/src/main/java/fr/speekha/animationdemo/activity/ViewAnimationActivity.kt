package fr.speekha.animationdemo.activity

import android.os.Bundle
import android.util.Log
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import fr.speekha.animationdemo.R
import fr.speekha.extensions.app.startActivity
import kotlinx.android.synthetic.main.xml_anim_activity.*

class ViewAnimationActivity : AnimationActivity() {

    // Since API 1
    // android.view.animation
    // only animates views
    // limited possibilities (only a few exposed properties like scaling or rotation,
    // but not background color for instance)
    // Doesn't affect actual view properties
    // Also supports frame by frame animation with a XML file that would list ALL frames
    // https://developer.android.com/guide/topics/graphics/view-animation.html
    // Some parts might be a bit buggy (especially with sets)
    // http://graphics-geek.blogspot.fr/2011/08/mysterious-behavior-of-fillbefore.html

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.xml_anim_activity)

        val xmlAnim = AnimationUtils.loadAnimation(applicationContext, R.anim.blink)
        xmlAnim.setAnimationListener(AnimListener("Xml animation"))

        val codeAnim = buildAnimation()
        codeAnim.setAnimationListener(AnimListener("Code animation"))

        start.setOnClickListener {
            xmlAnimated.startAnimation(xmlAnim)
            codeAnimated.startAnimation(codeAnim)
        }

        stop.setOnClickListener {
            xmlAnimated.clearAnimation()
            codeAnim.cancel()
        }

        next.setOnClickListener {
            startActivity<ValueAnimatorActivity>()
        }
    }

    private fun buildAnimation() = AlphaAnimation(1.0f, 0.0f).apply {
        duration = resources.getInteger(R.integer.anim_duration).toLong()
        repeatCount = Animation.INFINITE
        repeatMode = Animation.REVERSE
        isFillEnabled =  true
        fillBefore = true
        fillAfter = true
    }

    inner class AnimListener(val tag: String) : Animation.AnimationListener {
        override fun onAnimationStart(animation: Animation?) {
            Log.d(tag, "Start")
        }

        override fun onAnimationRepeat(animation: Animation?) {
            Log.d(tag, "Repeat")
        }

        override fun onAnimationEnd(animation: Animation?) {
            Log.d(tag, "End")
        }
    }
}
