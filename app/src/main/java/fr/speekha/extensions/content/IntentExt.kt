package fr.speekha.extensions.content

import android.content.Intent
import android.os.Parcelable

fun Intent.setExtras(args: Array<out Pair<String, Any>>) {
    args.forEach {
        val (key, value) = it
        when (value) {
            is String -> putExtra(key, value)
            is Int -> putExtra(key, value)
            is Double -> putExtra(key, value)
            is Boolean -> putExtra(key, value)
            is Parcelable -> putExtra(key, value)
            is IntArray -> putExtra(key, value)
            is DoubleArray -> putExtra(key, value)
            is BooleanArray -> putExtra(key, value)
            is Array<*> -> putExtra(key, value)
        }
    }
}