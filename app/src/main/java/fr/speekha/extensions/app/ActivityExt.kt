package fr.speekha.extensions.app

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import fr.speekha.extensions.content.setExtras

@Suppress("UNCHECKED_CAST")
fun <T : Any> Activity.getExtra(key: String) = intent.extras.get(key) as T

inline fun <reified T : Activity> Activity.startActivity(vararg args: Pair<String, Any>, options: Bundle? = null) {
    val intent = prepareIntent<T>(args)
    if (options != null) {
        startActivity(intent, options)
    } else {
        startActivity(intent)
        overridePendingTransition(0, 0)
    }
}

inline fun <reified T : Activity> Activity.startActivity(options: Bundle? = null) {
    val intent = Intent(this, T::class.java)
    if (options != null) {
        startActivity(intent, options)
    } else {
        startActivity(intent)
        overridePendingTransition(0, 0)
    }
}

inline fun <reified T : Activity> Activity.prepareIntent(args: Array<out Pair<String, Any>>) = Intent(this, T::class.java).apply { setExtras(args) }